## Technologies used

- pip: ``sudo apt-get install python3-pip``
- virtualenv: ``sudo pip3 install virtualenv``
- Python 3.8
- [Albion Scoreboard API](https://www.tools4albion.com/api_info.php)

## Creating and activating virtualenv
On the root of the project:``virtualenv venv`` and then ``source venv/bin/activate``

## Installing dependencies
``pip install -r requirements.txt``

## Enviroment variables
- DISCORD_TOKEN: Can be obtained by [registering a new bot in discord](https://discordpy.readthedocs.io/en/latest/discord.html).
if you are developing for an existing instance of the bot ask for the token.
- GUILD_ID: Can be obtained using Albion API. Imagine that we want to add the guild Yellow Bananas, the search would be: https://gameinfo.albiononline.com/api/gameinfo/search?q=Yellow Bananas