import aiohttp
import os
from logging import getLogger


class AlbionClient:
    def __init__(self):
        self._session = aiohttp.ClientSession()
        self._base_url = os.environ["ALBION_API_BASE_URL"]
        self._guild_id = os.environ["GUILD_ID"]
        self._logger = getLogger("AlbionClient")

    async def get_guild_info(self):
        async with self._session as session:
            get_guild_url = self._base_url + "guilds/" + self._guild_id
            async with session.get(get_guild_url) as request:
                self._logger.info(f'Making request to: {get_guild_url}...')
                self._logger.info(f'Request status for: {get_guild_url} was {request.status}')
                return await request.json()
