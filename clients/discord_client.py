import discord
import os

from logging import getLogger


class DiscordClient:
    def __init__(self):
        self.client = discord.Client()
        self._logger = getLogger("DiscordClient")

    def start_discord_listener_loop(self):
        self.client.run(os.environ["DISCORD_TOKEN"])
