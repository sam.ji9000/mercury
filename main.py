from clients.discord_client import DiscordClient
from clients.albion_client import AlbionClient
from actions.bot_request_reply import BotRequestReply
import logging
logging.basicConfig(level=logging.INFO)

a_client = AlbionClient()
d_client = DiscordClient()

bot_request_reply = BotRequestReply(albion_client=a_client, discord_client=d_client)

bot_request_reply.start_discord_listen_loop()




