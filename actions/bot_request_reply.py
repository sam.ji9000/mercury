from clients.albion_client import AlbionClient
from clients.discord_client import DiscordClient
import json
from logging import getLogger

class BotRequestReply:
    def __init__(self, albion_client: AlbionClient, discord_client: DiscordClient):
        self._albion_client = albion_client
        self._discord_client = discord_client
        self.d_client = discord_client.client
        self._logger = getLogger("BotRequestReply")

    def _register_request_listeners(self):
        @self._discord_client.client.event
        async def on_ready():
            self._logger.info(f'We have logged in as {self._discord_client.client.user}')

        @self._discord_client.client.event
        async def on_message(message):
            if message.author == self._discord_client.client.user:
                return

            if "eo" in message.content:
                await message.channel.send('Tirororo!')
            if "who are you?" in message.content:
                await message.channel.send("I'm fabulosity incarnate :nail_care:")
            if "what is your guild?" in message.content:
                guild_info = await self._albion_client.get_guild_info()
                guild_info = json.dumps(guild_info, indent=2)
                await message.channel.send(guild_info)

    def start_discord_listen_loop(self):
        self._register_request_listeners()
        self._discord_client.start_discord_listener_loop()
